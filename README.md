# REST API dla strony internetowej hotelu
<b>Port aplikacji: 8443</b>
<br>Jeśli aplikacja internetowa nie pobiera danych, należy zaakceptować sertyfikat SSL na stronie ''https://promont.wroc.pl:8443/api/rooms/help''.
<br>Jeśli wyświetli się informacja "Hotel api works" oznacza to, że aplikacja powinna działać.

## Wdrożenie
### Generowanie pliku .jar
Aby wygenerować plik .jar należy użyć następujących poleceń:
<br>
- Czyszczenie projektu:
````
.\mvnw clean:clean
````
- Zbudowanie pliku jar:
````
.\mvnw install
````
Zbudowany plik znajduje się w folderze `./target/`.

### Uruchomienie aplikacji
Do poprawnego działania aplikacji, baza danych powinna znajdować się w tym samym folderze co plik .jar.
- Uruchomienie:
````
java -jar HotelRestApi-1.0.0.jar
````

## Dokumentacja
Szczegółowa dokumentacja funkcji intefejsu REST dostępna jest pod adresem: https://adresSerwera:8443/swagger-ui.html.

## Autorzy

- Edyta Rogula
- Klaudia Gora
- Andrzej Miazga