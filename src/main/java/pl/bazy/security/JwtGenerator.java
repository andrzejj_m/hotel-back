package pl.bazy.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Component;
import pl.bazy.database.DbLoginController;
import pl.bazy.model.JwtUser;

import java.util.Date;

@Component
public class JwtGenerator {

    public String generate(JwtUser jwtUser) {
        String pass = "";
        try {
            DbLoginController dbLoginController = new DbLoginController();
            pass = dbLoginController.login(jwtUser);
            dbLoginController.disconnet();

            if(BCrypt.checkpw(jwtUser.getPassword(), pass)) {
                Claims claims = Jwts.claims()
                        .setSubject(jwtUser.getEmail());

                Date date = new Date(System.currentTimeMillis());

                pass = "";
                return Jwts.builder()
                        .setClaims(claims)
                        .signWith(SignatureAlgorithm.HS512, "wiadroCegieunaBa$enie")
                        .compact();
            }
        } catch (Exception e) {
            System.out.println("Błąd wyszukiwania użytkownika.");
        }

        return null;
    }
}
