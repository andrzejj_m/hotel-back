package pl.bazy.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.bazy.database.DbController;
import pl.bazy.model.Address;
import pl.bazy.model.Client;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@Api(description = "Podglądanie listy adresów i użytkowników", tags = {"Debug Controller"})
public class DebugController {



    //================ METODY PONIŻEJ NALEŻY USUNĄĆ NA KONIEC PROJEKTU================

    @ApiOperation("Listowanie użytkowników - DEBUG")
    @GetMapping("/getAllUsers")
    public List<Client> getAllClients() {
        List<Client> clientsList = new ArrayList<>();
        DbController dbController = new DbController();;

        try {
            clientsList = dbController.getAllClients();
            return clientsList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dbController.disconnet();
        return null;
    }

    @ApiOperation("Listowanie adresów - DEBUG")
    @GetMapping("/getAllAddresses")
    public List<Address> getAllAddresses() {
        List<Address> addressesList = new ArrayList<>();
        DbController dbController = new DbController();

        try {
            addressesList = dbController.getAllAddresses();
            return addressesList;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dbController.disconnet();
        return null;
    }

}
