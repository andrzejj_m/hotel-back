package pl.bazy.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.bazy.database.DbLoginController;
import pl.bazy.model.Client;
import pl.bazy.model.JwtUser;
import pl.bazy.security.JwtGenerator;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(description = "Logowanie i rejestracja", tags = {"Login Controller"})
public class LoginController {

    @ApiOperation("Rejestracja użytkownika")
    @PostMapping("/registerClient")
    public String registerCilent(@ApiParam(value = "Dane klienta i adresu") @RequestBody final Client client) {
        String addressId = "";
        String clientId = "";

        DbLoginController dbLoginController = new DbLoginController();

        try {
            addressId = dbLoginController.registerAddress(client.getAddress());
            clientId = dbLoginController.registerClient(client, addressId);
            return clientId;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbLoginController.disconnet();
        }

        dbLoginController.disconnet();
        return null;
    }

    @ApiOperation("Wygenerowanie tokena")
    @PostMapping(value = "/token")
    public Map<String, String> generate(@ApiParam(value = "Dane logowania") @RequestBody final JwtUser jwtUser) {
        JwtGenerator jwtGenerator = new JwtGenerator();
        Map<String, String> map = new HashMap<>();
        map.put("token", jwtGenerator.generate(jwtUser));

        return map;
    }
}
