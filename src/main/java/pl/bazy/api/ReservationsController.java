package pl.bazy.api;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import pl.bazy.database.DbLoginController;
import pl.bazy.database.DbReservationsController;
import pl.bazy.database.DbRoomsController;
import pl.bazy.model.Reservation;

import java.security.Principal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/rest/reservations")
@Api(description = "Zarządzanie rejestracjami", tags = {"Reservations Controller"})
public class ReservationsController {

    @ApiOperation(value = "Dodanie rezerwacji")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")})
    @PostMapping("/addReservation")
    public Map<String, String> addReservation(@ApiParam("Obiekt rezerwacji") @RequestBody final Reservation reservation, Principal principal) {
        Map<String, String> response = new HashMap<>();
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date creationDate = Calendar.getInstance().getTime();
        String date_now = dateFormat.format(creationDate);
        reservation.setCreationDate(date_now);
        
        if (countDays(date_now, reservation.getArrival()) < 0){
            response.put("text", "Nie mozna rozpoczac rezerwacji, ktora juz sie odbyla");
            return response;
        }
    
        if (!checkAvailability(reservation)){
            response.put("text", "Pokoj jest zajety w tym terminie");
            return response;
        }
        
        int payment = countPayment(reservation);
        if (payment <= 0){
            response.put("text", "Rezerwacja jest za krotka.");
            return response;
        }
        reservation.setPayment(payment);
    
        int idClient = getClientIdByEmail(principal.getName());
        
        DbReservationsController dbReservationsController = new DbReservationsController();

        try {
            dbReservationsController.addReservation(reservation, idClient);
            response.put("text", Integer.toString(payment));
            return response;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dbReservationsController.disconnet();
        return null;
    }
    
    @ApiOperation(value = "Wyswietlenie rezerwacji klienta")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")})
    @GetMapping("/getReservations")
    public ArrayList<Reservation> getReservations(Principal principal) {
        int idClient = getClientIdByEmail(principal.getName());
        
        DbReservationsController dbReservationsController = new DbReservationsController();
        
        try {
            return dbReservationsController.getReservations(idClient);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dbReservationsController.disconnet();
        
        return null;
    }
    
    @ApiOperation(value = "Anulowanie rezerwacji")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")})
    @PostMapping("/cancelReservation")
    public Map<String, String> cancelReservation(@ApiParam("Obiekt rezerwacji") @RequestBody final Reservation reservation, Principal principal) {
        DbReservationsController dbReservationsController = new DbReservationsController();
        Map<String, String> response = new HashMap<>();
    
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date cancelationDate = Calendar.getInstance().getTime();
        String cancel_date = dateFormat.format(cancelationDate);
        
        if (countDays(cancel_date, reservation.getArrival()) <= 3){
            response.put("text", "Nie mozna anulowac rezerwacji.");
            return response;
        }
    
        reservation.setCancelationDate(cancel_date);
        try {
            String result = dbReservationsController.cancelReservation(reservation);
            response.put("text", result);
            return response;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dbReservationsController.disconnet();

        return null;
    }
    
    private boolean checkAvailability(Reservation reservation){
        boolean isAvailable = false;
        DbRoomsController dbRoomsController = new DbRoomsController();
        try {
            dbRoomsController = new DbRoomsController();
            isAvailable = dbRoomsController.checkAvailabilityRoom(reservation.getArrival(), reservation.getDeparture(), reservation.getRoomId());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbRoomsController.disconnet();
        }

        dbRoomsController.disconnet();
        return isAvailable;
    }
    
    private int getClientIdByEmail(String email){
        DbLoginController dbLoginController = null;
        String clientId = "-1";
    
        try {
            dbLoginController = new DbLoginController();
            clientId = dbLoginController.getClientIdByEmail(email);
            return Integer.parseInt(clientId);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbLoginController.disconnet();
        }

        dbLoginController.disconnet();
        return -1;
    }
    
    private int countPayment(Reservation reservation) {
        int price = getRoomPrice(reservation.getRoomId());
        int days = countDays(reservation.getArrival(), reservation.getDeparture());
        
        if (days < 1) {
            return -1;
        }
    
        return days * price;
    }
    
    private int getRoomPrice(int roomId){
        int price = -1;
        DbRoomsController dbRoomsController = new DbRoomsController();
        try {
            dbRoomsController = new DbRoomsController();
            price = dbRoomsController.getPriceById(roomId);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbRoomsController.disconnet();
        }
        dbRoomsController.disconnet();
        return price;
    }
    
    private int countDays(String begining, String end){
        int days = 0;
        try {
            Date arrival = new SimpleDateFormat("yyyy-MM-dd").parse(begining);
            Date departure = new SimpleDateFormat("yyyy-MM-dd").parse(end);
            long diff = departure.getTime() - arrival.getTime();
            days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }

}
