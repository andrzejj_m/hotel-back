package pl.bazy.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import pl.bazy.database.DbUserController;
import pl.bazy.model.Client;
import pl.bazy.model.PassUpdate;

import java.security.Principal;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/rest/userDetails")
@Api(description = "Metody edycji użytkownika", tags = {"User Controller"})
public class UserController {

    @ApiOperation("Pobranie danych użytkownika")
    @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")
    @GetMapping("/getUserDetails")
    public Client getUserDetails(Principal principal) {
        DbUserController dbUserController = new DbUserController();

        try {
            return dbUserController.getUserDetails(principal.getName());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        dbUserController.disconnet();
        return null;
    }

    @ApiOperation("Edycja danych użytkownika")
    @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")
    @PostMapping("/updateUser")
    public Map<String, String> updateUser(@ApiParam("Obiekt reprezentujący klienta") @RequestBody final Client client, Principal principal) {
        DbUserController dbUserController = new DbUserController();
        Map<String, String> response = new HashMap<>();
        try {
            response.put("text", dbUserController.updateUser(client, principal.getName()));
            return response;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @ApiOperation("Usuwanie użytkownika z bazy")
    @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")
    @DeleteMapping("/deleteUser")
    public Map<String, String> deleteUser(Principal principal) {
        DbUserController dbUserController = new DbUserController();
        Map<String, String> response = new HashMap<>();

        try {
            response.put("text", dbUserController.deleteUser(principal.getName()));
            return response;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @ApiOperation("Zmiana hasła")
    @ApiImplicitParam(name = "Authorization", value = "Token autoryzacyjny", required = true, dataType = "String", paramType = "header")
    @PostMapping("/updatePassword")
    public Map<String, String> updatePassword(@RequestBody final PassUpdate passUpdate, Principal principal) {
        DbUserController dbUserController = new DbUserController();
        Map<String, String> response = new HashMap<>();

        try {
            response.put("text", dbUserController.updatePassword(passUpdate, principal.getName()));
            return response;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
