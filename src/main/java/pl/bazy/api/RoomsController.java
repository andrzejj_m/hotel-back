package pl.bazy.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;
import pl.bazy.database.DbRoomsController;
import pl.bazy.model.Availability;
import pl.bazy.model.Room;

import java.sql.SQLException;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/rooms")
@Api(description = "Zarządzanie pokojami", tags = {"Rooms Controller"})
public class RoomsController {
    
    @ApiOperation("Sprawdzenie czy api odpowiada")
    @GetMapping("/help")
    public String help() {
        return "Hotel api works.";
    }
    
    @ApiOperation("Pobranie wszystkich pokoi")
    @GetMapping("/getAllRooms")
    public ArrayList<Room> getAllRooms() {
        ArrayList<Room> roomsList = new ArrayList<>();
        DbRoomsController dbRoomsController = new DbRoomsController();
        
        try {
            roomsList = dbRoomsController.getAllRooms();
            return roomsList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbRoomsController.disconnet();
        }

        dbRoomsController.disconnet();
        return null;
    }
    
    @ApiOperation("Pobranie dostepnych pokoi danego typu w danym terminie")
    @PostMapping("/checkAvailability")
    public ArrayList<Room> getAvailableRooms(@ApiParam("Obiekt reprezentujący dostępność") @RequestBody final Availability availabililty) {
        ArrayList<Room> roomsList = new ArrayList<>();
        DbRoomsController dbRoomsController = new DbRoomsController();
        
        try {
            roomsList = dbRoomsController.checkAvailabilityRoomType(availabililty.getArrival(),
                    availabililty.getDeparture(), dbRoomsController.getRoomTypeId(availabililty.getRoomType()));
            return roomsList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            dbRoomsController.disconnet();
        }

        dbRoomsController.disconnet();
        return null;
    }
}
