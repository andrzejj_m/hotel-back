package pl.bazy.database;

import org.mindrot.jbcrypt.BCrypt;
import pl.bazy.model.Address;
import pl.bazy.model.Client;
import pl.bazy.model.PassUpdate;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DbUserController extends DbController {

    public Client getUserDetails(String email) throws SQLException {
        String SQLline = "SELECT * FROM Clients WHERE email LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, email);

        ResultSet resultSet = preparedStatement.executeQuery();
        Client client = new Client();

        int addressID = -1;

        while (resultSet.next()) {
            client.setIdClient(resultSet.getInt("ID_client"));
            client.setEmail(resultSet.getString("email"));
            client.setFirstName(resultSet.getString("first_name"));
            client.setLastName(resultSet.getString("last_name"));
            client.setIdentyficationCard(resultSet.getString("identyfication_card"));
            client.setPhone(resultSet.getString("phone"));
            addressID = resultSet.getInt("ID_address");
        }

        SQLline = "SELECT * FROM Addresses WHERE ID_address = ?;";

        preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setInt(1, addressID);

        resultSet = preparedStatement.executeQuery();
        Address address = new Address();

        while (resultSet.next()) {
            address.setIdAddress(resultSet.getInt("ID_address"));
            address.setCity(resultSet.getString("city"));
            address.setStreet(resultSet.getString("street"));
            address.setHouseNr(resultSet.getString("house_nr"));
            address.setApartamentNr(resultSet.getString("apartament_nr"));
            address.setPostalCode(resultSet.getString("postal_code"));
            address.setCountry(resultSet.getString("country"));
        }

        client.setAddress(address);

        return client;
    }

    public String updateUser(Client client, String email) throws SQLException {
        DbLoginController dbLoginController = new DbLoginController();
        String addressId = dbLoginController.registerAddress(client.getAddress());


        String SQLline = "UPDATE clients SET " +
                "first_name = ?, last_name = ?, identyfication_card = ?, phone = ?, email = ?, ID_address = ? " +
                "WHERE email LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, client.getFirstName());
        preparedStatement.setString(2, client.getLastName());
        preparedStatement.setString(3, client.getIdentyficationCard());
        preparedStatement.setString(4, client.getPhone());
        preparedStatement.setString(5, client.getEmail());
        preparedStatement.setInt(6, Integer.parseInt(addressId));
        preparedStatement.setString(7, email);

        int result = preparedStatement.executeUpdate();

        if(result > 0) {
            return "Zaktualizowano";
        } else {
            return null;
        }
    }

    public String updatePassword(PassUpdate passUpdate, String email) throws SQLException {
        String SQLline = "SELECT * FROM Clients WHERE email LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, email);

        ResultSet resultSet = preparedStatement.executeQuery();
        String pass = "";

        while (resultSet.next()) {
            pass = resultSet.getString("hash");
        }

        if(BCrypt.checkpw(passUpdate.getOldPass(), pass)) {
            SQLline = "UPDATE Clients SET hash = ? WHERE email LIKE ?;";

            preparedStatement = connection.prepareStatement(SQLline);
            preparedStatement.setString(1, hashPass(passUpdate.getNewPass()));
            preparedStatement.setString(2, email);

            int response = preparedStatement.executeUpdate();

            if(response == 1) {
                return "Zaktualizowano";
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public String deleteUser(String email) throws SQLException {
    String SQLline = "UPDATE Reservations " +
                "SET ID_client = null " +
                "WHERE ID_client = (SELECT ID_client FROM Clients WHERE email LIKE ?);";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, email);

        int result = preparedStatement.executeUpdate();


        SQLline = "DELETE FROM Clients WHERE email LIKE ?;";

        preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, email);

        result += preparedStatement.executeUpdate();

        if(result > 1) {
            return "Usunięto";
        } else {
            return null;
        }
    }

    private String hashPass(String pass) {
        return BCrypt.hashpw(pass, BCrypt.gensalt());
    }
}
