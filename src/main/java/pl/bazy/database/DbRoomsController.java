package pl.bazy.database;

import pl.bazy.model.Room;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DbRoomsController extends DbController{

    public ArrayList<Room> getAllRooms() throws SQLException {
        ArrayList<Room> roomsList = new ArrayList<>();

        String SQLline = "SELECT Rooms.ID_room, RoomTypes.type, Rooms.long_description, Rooms.price, Rooms.is_open " +
                "FROM Rooms " +
                "INNER JOIN RoomTypes ON Rooms.ID_roomtype = RoomTypes.ID_roomtype";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        ResultSet resultSet = preparedStatement.executeQuery();

        Room room = null;
        while(resultSet.next()) {
            room = new Room(
                    resultSet.getInt("ID_room"),
                    resultSet.getString("type"),
                    resultSet.getString("long_description"),
                    resultSet.getInt("price"),
                    resultSet.getBoolean("is_open")
            );
            roomsList.add(room);
        }
        resultSet.close();

        return roomsList;
    }
    
    public boolean checkAvailabilityRoom(String arrival, String departure, int id_room) throws SQLException {
        String SQLline = "SELECT count(r.id_room) as num FROM Rooms r WHERE NOT EXISTS " +
                "( SELECT 1 FROM Reservations b WHERE b.id_room = r.id_room " +
                " AND (b.cancelation_date = \"\" OR b.cancelation_date IS NULL) " +
                "AND ( (? >= b.arrival AND ? < b.departure) " +
                "OR (? <= b.arrival AND ? >= b.departure) ) )" +
                "AND r.id_room = ?" +
                "AND is_open = 1;";
        
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, arrival);
        preparedStatement.setString(2, arrival);
        preparedStatement.setString(3, arrival);
        preparedStatement.setString(4, departure);
        preparedStatement.setInt(5, id_room);
        
        ResultSet resultSet = preparedStatement.executeQuery();
        
        int result = -1;
        while(resultSet.next()) {
            result = resultSet.getInt("num");
        }
        resultSet.close();
        
        return result > 0;
    }
    
    public ArrayList<Room> checkAvailabilityRoomType(String arrival, String departure, int id_roomtype) throws SQLException {
        String SQLline = "SELECT r.* FROM Rooms r WHERE NOT EXISTS " +
                "( SELECT 1 FROM Reservations b WHERE b.id_room = r.id_room " +
                " AND (b.cancelation_date = \"\" OR b.cancelation_date IS NULL) " +
                "AND ( (? >= b.arrival AND ? < b.departure) " +
                "OR (? <= b.arrival AND ? >= b.departure) ) )" +
                "AND r.id_roomtype = ?" +
                "AND is_open = 1;";
        
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, arrival);
        preparedStatement.setString(2, arrival);
        preparedStatement.setString(3, arrival);
        preparedStatement.setString(4, departure);
        preparedStatement.setInt(5, id_roomtype);
        
        ResultSet resultSet = preparedStatement.executeQuery();
        
        ArrayList<Room> roomsList = new ArrayList<>();
        Room room = null;
        while(resultSet.next()) {
            room = new Room(
                    resultSet.getInt("ID_room"),
                    resultSet.getString("ID_roomtype"),
                    resultSet.getString("long_description"),
                    resultSet.getInt("price"),
                    resultSet.getBoolean("is_open")
            );
            roomsList.add(room);
        }
        resultSet.close();
        
        return roomsList;
    }
    
    public int getRoomTypeId(String roomType) throws SQLException{
        String SQLline = "SELECT ID_roomtype FROM RoomTypes WHERE type LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, roomType);

        ResultSet resultSet = preparedStatement.executeQuery();
        int id = -1;

        while (resultSet.next()) {
            id = resultSet.getInt("ID_roomtype");
        }

        return id;
    }
    
    public int getPriceById(int id_room) throws SQLException {
        String SQLline = "SELECT price FROM Rooms WHERE id_room = ?;";
    
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setInt(1, id_room);
    
        ResultSet resultSet = preparedStatement.executeQuery();
        int price = -1;
    
        while (resultSet.next()) {
            price = resultSet.getInt("price");
        }
    
        return price;
    }
}
