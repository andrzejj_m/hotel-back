package pl.bazy.database;

import pl.bazy.model.Address;
import pl.bazy.model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DbController {
    protected Connection connection;

    public DbController() {
        connect();
    }

    public void connect() {
        connection = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:HotelBase.db");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnet() {
        System.out.println("Zamykam połączenie z bazą...");
        try {
            if(!connection.isClosed()) {
                connection.close();
            }
            System.out.println("Zamknięte");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

                                //TYLKO DO DEBUGOWANIA - bez PrepareStatement
    //===========================================================================================
    public List<Client> getAllClients() throws SQLException {
        List<Client> clientsList = new ArrayList<>();
        String SQLline = "SELECT * FROM Clients;";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQLline);

        Client client;
        while (resultSet.next()) {
            client = new Client(
                    resultSet.getInt("ID_client"),
                    resultSet.getString("first_name"),
                    resultSet.getString("last_name"),
                    resultSet.getString("identyfication_card"),
                    resultSet.getString("phone"),
                    resultSet.getString("email"),
                    resultSet.getString("hash"),
                    null
            );
            clientsList.add(client);
        }
        statement.close();
        resultSet.close();

        return clientsList;
    }

    public List<Address> getAllAddresses() throws SQLException {
        List<Address> addressesList = new ArrayList<>();
        String SQLline = "SELECT * FROM Addresses;";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SQLline);

        Address address;
        while(resultSet.next()) {
            address = new Address(
                    resultSet.getInt("ID_address"),
                    resultSet.getString("city"),
                    resultSet.getString("street"),
                    resultSet.getString("house_nr"),
                    resultSet.getString("apartament_nr"),
                    resultSet.getString("postal_code"),
                    resultSet.getString("country")
            );
            addressesList.add(address);
        }
        statement.close();
        resultSet.close();

        return addressesList;
    }
    //===========================================================================================

}
