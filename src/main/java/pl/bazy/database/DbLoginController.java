package pl.bazy.database;

import org.mindrot.jbcrypt.BCrypt;
import pl.bazy.model.Address;
import pl.bazy.model.Client;
import pl.bazy.model.JwtUser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbLoginController extends DbController{

    public String registerAddress(Address address) throws SQLException {
        String id = getAddressId(address);
        connect();

        if(id == null || id == "") {
            String SQLline = "INSERT INTO Addresses (city, street, house_nr, apartament_nr, postal_code, country) " +
                    "VALUES (?, ?, ?, ?, ?, ?);";
            PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
            preparedStatement.setString(1, address.getCity());
            preparedStatement.setString(2, address.getStreet());
            preparedStatement.setString(3, address.getHouseNr());
            preparedStatement.setString(4, address.getApartamentNr());
            preparedStatement.setString(5, address.getPostalCode());
            preparedStatement.setString(6, address.getCountry());

            preparedStatement.executeUpdate();

            id = getAddressId(address);
        }

        return id;
    }

    public String getAddressId(Address address) throws SQLException {
        String SQLline = "SELECT ID_address FROM Addresses WHERE " +
                "city LIKE ? AND " +
                "street LIKE ? AND " +
                "house_nr LIKE ? AND " +
                "apartament_nr LIKE ? AND " +
                "postal_code LIKE ? AND " +
                "country LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, address.getCity());
        preparedStatement.setString(2, address.getStreet());
        preparedStatement.setString(3, address.getHouseNr());
        preparedStatement.setString(4, address.getApartamentNr());
        preparedStatement.setString(5, address.getPostalCode());
        preparedStatement.setString(6, address.getCountry());

        ResultSet resultSet = preparedStatement.executeQuery();

        String id = null;
        while(resultSet.next()) {
            id = resultSet.getString("ID_address");
        }
        resultSet.close();

        return id;
    }

    public String getClientId(Client client, String addressId) throws SQLException {
        String SQLline = "SELECT ID_client FROM Clients WHERE " +
                "first_name LIKE ? AND " +
                "last_name LIKE ? AND " +
                "identyfication_card LIKE ? AND " +
                "phone LIKE ? AND " +
                "email LIKE ? AND " +
                "ID_address LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, client.getFirstName());
        preparedStatement.setString(2, client.getLastName());
        preparedStatement.setString(3, client.getIdentyficationCard());
        preparedStatement.setString(4, client.getPhone());
        preparedStatement.setString(5, client.getEmail());
        preparedStatement.setString(6, addressId);

        ResultSet resultSet = preparedStatement.executeQuery();

        String id = null;
        while(resultSet.next()) {
            id = resultSet.getString("ID_client");
        }

        resultSet.close();

        return id;
    }
    
    public String getClientIdByEmail(String email) throws SQLException {
        String SQLline = "SELECT ID_client FROM Clients WHERE " +
                "email LIKE ?;";
        
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, email);
        
        ResultSet resultSet = preparedStatement.executeQuery();
        
        String id = null;
        while(resultSet.next()) {
            id = resultSet.getString("ID_client");
        }
        
        resultSet.close();
        
        return id;
    }

    public String registerClient(Client client, String addressId) throws SQLException {
        String SQLline = "INSERT INTO Clients (first_name, last_name, identyfication_card, phone, email, hash, ID_address) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?);";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, client.getFirstName());
        preparedStatement.setString(2, client.getLastName());
        preparedStatement.setString(3, client.getIdentyficationCard());
        preparedStatement.setString(4, client.getPhone());
        preparedStatement.setString(5, client.getEmail());
        preparedStatement.setString(6, hashPassword(client.getPassword()));
        preparedStatement.setString(7, addressId);

        preparedStatement.executeUpdate();

        String id = getClientId(client, addressId);

        return id;
    }

    public String login(JwtUser login) throws SQLException {
        String pass = null;

        String SQLline = "SELECT hash FROM Clients WHERE email LIKE ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, login.getEmail());

        ResultSet resultSet = preparedStatement.executeQuery();

        while(resultSet.next()) {
            pass = resultSet.getString("hash");
        }

        return pass;
    }


    private String hashPassword(String plainPass) {
        return BCrypt.hashpw(plainPass, BCrypt.gensalt());
    }
}
