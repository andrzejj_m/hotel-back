package pl.bazy.database;

import pl.bazy.model.Reservation;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class DbReservationsController extends DbController {

    public String addReservation(Reservation reservation, int id) throws SQLException {
        String SQLline = "INSERT INTO Reservations (creation_date, arrival, departure, payment, ID_client, ID_room, payment_date, cancelation_date) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, reservation.getCreationDate());
        preparedStatement.setString(2, reservation.getArrival());
        preparedStatement.setString(3, reservation.getDeparture());
        preparedStatement.setInt(4, reservation.getPayment());
        preparedStatement.setInt(5, id);
        preparedStatement.setInt(6, reservation.getRoomId());
        preparedStatement.setString(7, reservation.getPaymentDate());
        preparedStatement.setString(8, reservation.getCancelationDate());

        preparedStatement.executeUpdate();

        return "Dodano rezerwacje";
    }
    
    public ArrayList<Reservation> getReservations(int clientId) throws SQLException {
        ArrayList<Reservation> roomsList = new ArrayList<>();
    
        String SQLline = "SELECT Reservations.ID_reservation, Reservations.creation_date, Reservations.arrival, Reservations.departure, " +
                "Reservations.ID_client, Reservations.payment, Reservations.ID_room, Reservations.payment_date, Reservations.cancelation_date " +
                "FROM Reservations WHERE Reservations.ID_client = ?";
    
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setInt(1, clientId);
        ResultSet resultSet = preparedStatement.executeQuery();
    
        Reservation reservation = null;
        while(resultSet.next()) {
            reservation = new Reservation(
                    resultSet.getInt("ID_reservation"),
                    resultSet.getString("creation_date"),
                    resultSet.getString("arrival"),
                    resultSet.getString("departure"),
                    resultSet.getInt("payment"),
                    resultSet.getInt("ID_client"),
                    resultSet.getInt("ID_room"),
                    resultSet.getString("payment_date"),
                    resultSet.getString("cancelation_date")
            );
            roomsList.add(reservation);
        }
        resultSet.close();
    
        return roomsList;
    }
    
    public String cancelReservation(Reservation reservation) throws SQLException {
        String SQLline = "UPDATE Reservations SET cancelation_date = ? WHERE id_reservation = ?;";
    
        PreparedStatement preparedStatement = connection.prepareStatement(SQLline);
        preparedStatement.setString(1, reservation.getCancelationDate());
        preparedStatement.setInt(2, reservation.getReservationId());
    
        preparedStatement.executeUpdate();
    
        return "Anulowano rezerwacje";
    }
    
}
