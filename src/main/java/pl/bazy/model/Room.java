package pl.bazy.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Room {
    private int ID_room;
    private String type;
    private String long_description;
    private int price;
    private boolean is_open;

    public Room(int ID_room, String type, String long_description, int price, boolean is_open) {
        this.ID_room = ID_room;
        this.type = type;
        this.long_description = long_description;
        this.price = price;
        this.is_open = is_open;
    }

    public int getID_room() {
        return ID_room;
    }

    public void setID_room(int ID_room) {
        this.ID_room = ID_room;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLong_description() {
        return long_description;
    }

    public void setLong_description(String long_description) {
        this.long_description = long_description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isIs_open() {
        return is_open;
    }

    public void setIs_open(boolean is_open) {
        this.is_open = is_open;
    }
}
