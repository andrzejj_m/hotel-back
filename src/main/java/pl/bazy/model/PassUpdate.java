package pl.bazy.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class PassUpdate {
    private String email;
    private String oldPass;
    private String newPass;

    public PassUpdate() {
    }

    public PassUpdate(String email, String oldPass, String newPass) {
        this.email = email;
        this.oldPass = oldPass;
        this.newPass = newPass;
    }

    @Override
    public String toString() {
        return "PassUpdate{" +
                "email='" + email + '\'' +
                ", oldPass='" + oldPass + '\'' +
                ", newPass='" + newPass + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }
}
