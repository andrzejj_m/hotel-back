package pl.bazy.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Availability {
    String arrival;
    String departure;
    String RoomType;
    
    public Availability(){ }

    public Availability(String arrival, String departure, String roomType) {
        this.arrival = arrival;
        this.departure = departure;
        RoomType = roomType;
    }

    @Override
    public String toString() {
        return "Availability{" +
                "arrival='" + arrival + '\'' +
                ", departure='" + departure + '\'' +
                ", RoomType='" + RoomType + '\'' +
                '}';
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getRoomType() {
        return RoomType;
    }

    public void setRoomType(String roomType) {
        RoomType = roomType;
    }
}
