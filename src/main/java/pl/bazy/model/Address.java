package pl.bazy.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Objects;

@JsonAutoDetect
public class Address {
    private int IdAddress;
    private String city;
    private String street;
    private String houseNr;
    private String apartamentNr;
    private String postalCode;
    private String country;

    public Address() {
        setIdAddress(0);
    }

    public Address(int idAddress, String city, String street, String houseNr, String apartamentNr, String postalCode, String country) {
        IdAddress = idAddress;
        this.city = city;
        this.street = street;
        this.houseNr = houseNr;
        this.apartamentNr = apartamentNr;
        this.postalCode = postalCode;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Address{" +
                "IdAddress=" + IdAddress +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", houseNr='" + houseNr + '\'' +
                ", apartamentNr='" + apartamentNr + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public int getIdAddress() {
        return IdAddress;
    }

    public void setIdAddress(int idAddress) {
        IdAddress = idAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNr() {
        return houseNr;
    }

    public void setHouseNr(String houseNr) {
        this.houseNr = houseNr;
    }

    public String getApartamentNr() {
        return apartamentNr;
    }

    public void setApartamentNr(String apartamentNr) {
        this.apartamentNr = apartamentNr;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
