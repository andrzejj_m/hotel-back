package pl.bazy.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Client {
    private int IdClient;
    private String firstName;
    private String lastName;
    private String identyficationCard;
    private String phone;
    private String email;
    private String password;
    private Address address;

    public Client() {
        setIdClient(0);
    }

    public Client(int idClient, String firstName, String lastName, String identyficationCard, String phone, String email, String password, Address address) {
        this.IdClient = idClient;
        this.firstName = firstName;
        this.lastName = lastName;
        this.identyficationCard = identyficationCard;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Client{" +
                "IdClient=" + IdClient +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", identyficationCard='" + identyficationCard + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", address=" + address +
                '}';
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public int getIdClient() {
        return IdClient;
    }

    public void setIdClient(int idClient) {
        IdClient = idClient;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentyficationCard() {
        return identyficationCard;
    }

    public void setIdentyficationCard(String identyficationCard) {
        this.identyficationCard = identyficationCard;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
