package pl.bazy.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Reservation {

    private int reservationId;
    private String creationDate;
    private String arrival;
    private String departure;
    private int payment;
    private int clientId;
    private int roomId;
    private String paymentDate;
    private String cancelationDate;

    public Reservation() {
    }

    public Reservation(int reservationId, String creationDate, String arrival, String departure, int payment, int clientId, int roomId, String paymentDate, String cancelationDate) {
        this.reservationId = reservationId;
        this.creationDate = creationDate;
        this.arrival = arrival;
        this.departure = departure;
        this.payment = payment;
        this.clientId = clientId;
        this.roomId = roomId;
        this.paymentDate = paymentDate;
        this.cancelationDate = cancelationDate;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "reservationId=" + reservationId +
                ", creationDate='" + creationDate + '\'' +
                ", arrival='" + arrival + '\'' +
                ", departure='" + departure + '\'' +
                ", payment='" + payment + '\'' +
                ", clientId='" + clientId + '\'' +
                ", roomId=" + roomId +
                ", paymentDate='" + paymentDate + '\'' +
                ", cancelationDate='" + cancelationDate + '\'' +
                '}';
    }

    public int getReservationId() {
        return reservationId;
    }

    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }
    
    public int getPayment() {
        return payment;
    }
    
    public void setPayment(int payment) {
        this.payment = payment;
    }
    
    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getCancelationDate() {
        return cancelationDate;
    }

    public void setCancelationDate(String cancelationDate) {
        this.cancelationDate = cancelationDate;
    }
}

